# SkillAvid Pure black theme for VS Code

This Pure black theme is designed with vibrant hand picked colors that ensures peace of your eyes.

## Screenshots
Midnight Black
<br>
![Midnight_Black!](https://gitlab.com/redwan-hossain/skillavid-pure-black-vscode-theme/-/raw/main/img/midnight.png) <br> <br>


Pure Black
<br>
![Pure_Black!](https://gitlab.com/redwan-hossain/skillavid-pure-black-vscode-theme/-/raw/main/img/black.png) <br> <br>


## Recommended Font

Font name:- Fantasque Sans Mono (Large line height, no loop k) <br><br>
Download the font from here:- https://github.com/belluzj/fantasque-sans/releases

## Recommended settings.json

```json
{
  "terminal.integrated.tabs.enabled": true,
  "files.autoSave": "afterDelay",
  "editor.tabSize": 2,
  "editor.fontSize": 19,
  "editor.fontWeight": "600",
  "editor.lineHeight": 27,
  "editor.letterSpacing": 0.9,
  "editor.wordWrap": "on",
  "editor.fontFamily": "Fantasque Sans Mono",
  "editor.fontLigatures": true,
  "terminal.integrated.fontFamily": "Fantasque Sans Mono",
  "terminal.integrated.fontSize": 15,
  "editor.cursorSmoothCaretAnimation": true,
  "editor.cursorBlinking": "expand",
  "telemetry.enableTelemetry": false,
  "editor.linkedEditing": true,
  "files.trimTrailingWhitespace": true,
  "editor.bracketPairColorization.enabled": true
}
```

## License

This software is released under [MIT License](http://www.opensource.org/licenses/mit-license.php)
© Md. Redwan Hossain
